(function ($) {
  /**
   * Source callback for autocomplete that performs a remote search.
   */
  function contact_search(request, response) {
    var throbber = $(this.element).siblings('.sugarcrm-contacts-throbber');
    throbber.show();
    $.ajax({
      url: Drupal.settings.sugarcrm_contacts_search_url,
      dataType: "jsonp",
      data: {
        term: request.term
      },
      success: function(data) {
        throbber.hide();
        response(data);
      }
    });
  }

  /**
   * Sets the sugar id as a hidden field when a contact is selected.
   */
  function contact_select(e, ui) {
    var sugarid_field = $('#' + $(this).data('sugarid-field-id'));
    if (ui.item.sugarid == 'noresults') {
      sugarid_field.val('');
      return false;
    }
    sugarid_field.val(ui.item.sugarid);
    $(this).parent().siblings('.sugarcrm-contacts-extra-field')
      .find('.sugarcrm-contacts-value')
      .css('text-decoration', 'line-through');
  }

  /**
   * Triggers the search for the input associated with the clicked button.
   */
  function trigger_search() {
    var input = $('#' + $(this).data('input_id'));
    input.autocomplete('option', 'minLength', 0);
    input.autocomplete('search');
    input.autocomplete('option', 'minLength', 9999);
  }

  /**
   * Drupal behavior that initializes the contact search functionality.
   */
  Drupal.behaviors.sugarcrmContactsSearch = {
    attach: function (context, settings) {
      // Append a button to trigger the search.
      $('.sugarcrm-contacts-name').each(function (i, e) {
        var button = $('<a>' + Drupal.t('Search') + '</a>')
          .attr('href', '#')
          .addClass('sugarcrm-contacts-search')
          .data('input_id', $(this).attr('id'))
          .click(trigger_search);
        var throbber = $('<span>' + Drupal.t('Searching...') + '</span>')
          .addClass('sugarcrm-contacts-throbber')
          .hide();
        $(this).after(throbber);
        $(this).after(button);
      });

      // Initialize autocomplete.
      $('.sugarcrm-contacts-name').autocomplete({
        source: contact_search,
        select: contact_select,
        // We don't want the autocomplete to trigger on user input, only after
        // the search button is pressed.
        minLength: 9999,
        html: true
      });
    }
  };
})(jQuery);
