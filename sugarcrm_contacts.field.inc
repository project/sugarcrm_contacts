<?php

/**
 * @file
 * Field API hooks.
 */

/**
 * Implements hook_field_info().
 */
function sugarcrm_contacts_field_info() {
  return array(
    'sugarcrm_contacts' => array(
      'label' => t('SugarCRM Contact'),
      'description' => t('Reference a SugarCRM contact.'),
      'default_widget' => 'sugarcrm_contacts',
      'default_formatter' => 'sugarcrm_contacts_info',
      'property_type' => 'field_item_sugarcrm_contact',
      'property_callbacks' => array('sugarcrm_contacts_field_property_callback'),
    ),
  );
}

/**
 * Implements hook_field_validate().
 */
function sugarcrm_contacts_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  module_load_include('inc', 'sugarcrm_contacts', 'sugarcrm_contacts.sugar');
  foreach ($items as $delta => $item) {
    $sugar_id = $item['contact']['sugar_id'];
    if (empty($item['contact']['name']) && empty($sugar_id)) {
      continue;
    }

    if (!sugarcrm_contacts_is_valid_id($sugar_id)) {
      $errors[$field['field_name']][$langcode][$delta][] = array(
        'error' => 'sugarcrm_contacts_invalid_id',
        'message' => t('%name: %id is not a valid sugar id.', array('%name' => $instance['label'], '%id' => $sugar_id)),
      );
    }
  }
}


/**
 * Implements hook_field_is_empty().
 *
 * We consider the field empty if there is no sugar_id.
 */
function sugarcrm_contacts_field_is_empty($item, $field) {
  return empty($item['contact']['sugar_id']);
}

/**
 * Implements hook_field_formatter_info().
 */
function sugarcrm_contacts_field_formatter_info() {
  return array(
    'sugarcrm_contacts_info' => array(
      'label' => t('SugarCRM Contact information.'),
      'field types' => array('sugarcrm_contacts'),
      'settings' => array(
        'contact_fields' => array(),
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function sugarcrm_contacts_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $element = array();
  if ($display['type'] == 'sugarcrm_contacts_info') {
    module_load_include('inc', 'sugarcrm_contacts', 'sugarcrm_contacts.sugar');
    $element['contact_fields'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Contact fields'),
      '#description' => t('Select the extra fields to show (name is always shown).'),
      '#options' => _sugarcrm_contacts_sugar_fields(),
      '#default_value' => isset($settings['contact_fields']) ? $settings['contact_fields'] : array(),
    );
  }
  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function sugarcrm_contacts_field_formatter_settings_summary($field, $instance, $view_mode) {
  module_load_include('inc', 'sugarcrm_contacts', 'sugarcrm_contacts.sugar');
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  if ($display['type'] == 'sugarcrm_contacts_info') {
    $show = array('Name');
    $fields = _sugarcrm_contacts_sugar_fields();
    if (!empty($settings['contact_fields'])) {
      foreach ($settings['contact_fields'] as $field) {
        if ($field) {
          $show[] = $fields[$field];
        }
      }
    }
  }
  return t('Showing %fields.', array('%fields' => join(', ', $show)));
}

/**
 * Implements hook_field_formatter_view().
 */
function sugarcrm_contacts_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  $settings = $display['settings'];

  switch ($display['type']) {
    case 'sugarcrm_contacts_info':
      foreach ($items as $delta => $item) {
        $element[$delta]['name'] = array(
          '#prefix' => '<div class="sugarcrm-contact-name">',
          '#markup' => $item['name'],
          '#suffix' => '</div>',
        );
        $element[$delta]['extra'] = sugarcrm_contacts_get_display_extra_fields($item['sugar_id'], $settings);
      }
      break;
  }

  return $element;
}

/**
 * Implements hook_field_widget_info().
 */
function sugarcrm_contacts_field_widget_info() {
  return array(
    'sugarcrm_contacts' => array(
      'label' => t('SugarCRM Contact select'),
      'description' => t('Search and select the contact.'),
      'field types' => array('sugarcrm_contacts'),
      'settings' => array(
        'sugarcrm_contacts' => array(
          'contact_fields' => array(),
        ),
      ),
    ),
  );
}

/**
 * Implements hook_field_widget_settings_form().
 */
function sugarcrm_contacts_field_widget_settings_form($field, $instance) {
  $widget = $instance['widget'];
  $settings = $widget['settings']['sugarcrm_contacts'];

  $element = array();
  if ($widget['type'] == 'sugarcrm_contacts') {
    module_load_include('inc', 'sugarcrm_contacts', 'sugarcrm_contacts.sugar');
    $element['sugarcrm_contacts'] = array(
      '#type' => 'fieldset',
      '#title' => t('SugarCRM Contacts'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#tree' => TRUE,
    );
    $element['sugarcrm_contacts']['contact_fields'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Contact fields'),
      '#description' => t('Select the extra contact fields to show in the widget next to the contact name.'),
      '#options' => _sugarcrm_contacts_sugar_fields(),
      '#default_value' => isset($settings['contact_fields']) ? $settings['contact_fields'] : array(),
    );
  }
  return $element;
}

/**
 * Implements hook_field_widget_form().
 */
function sugarcrm_contacts_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $value = isset($items[$delta]) ? $items[$delta] : array();

  $widget = $element;
  $widget['#delta'] = $delta;

  switch ($instance['widget']['type']) {
    case 'sugarcrm_contacts':
      $widget += array(
        '#type' => 'fieldset',

        // #delta is set so that the validation function will be able
        // to access external value information which otherwise would be
        // unavailable.
        '#delta' => $delta,
        '#attributes' => array(
          'class' => array('field-sugarcrm-contact-container'),
        ),
      );

      $sugarid_field_id = drupal_html_id($field['field_name'] . '-sugarid-' . $delta);
      $widget['name'] = array(
        '#type' => 'textfield',
        '#title' => t('Contact name'),
        '#title_display' => 'invisible',
        '#description' => t('Search contacts by name in @sugarcrm_instance and select the one you want to reference.', array('@sugarcrm_instance' => variable_get('sugarcrm_name', 'SugarCRM'))),
        '#default_value' => isset($value['name']) ? $value['name'] : '',
        '#attributes' => array(
          'class' => array('sugarcrm-contacts-name'),
          'data-sugarid-field-id' => $sugarid_field_id,
        ),
      );
      $sugarid = isset($value['sugar_id']) ? $value['sugar_id'] : '';
      $widget['sugar_id'] = array(
        '#type' => 'hidden',
        '#title' => t('Sugar ID'),
        '#default_value' => $sugarid,
        '#attributes' => array(
          'class' => array('sugarcrm-contacts-sugar-id'),
          'id' => $sugarid_field_id,
        ),
      );
      // Add extra contact fields if a contact is already selected.
      if (!empty($sugarid)) {
        $settings = $instance['widget']['settings']['sugarcrm_contacts'];
        $extra_fields = sugarcrm_contacts_get_widget_extra_fields($sugarid, $settings);
        $widget = array_merge($widget, $extra_fields);
      }
      break;
  }

  drupal_add_library('system', 'ui.autocomplete');
  drupal_add_js(drupal_get_path('module', 'sugarcrm_contacts') . '/js/sugarcrm_contacts.search.js');
  drupal_add_js(array('sugarcrm_contacts_search_url' => url('sugarcrm-contacts/search', array('absolute' => TRUE))), 'setting');
  $element['contact'] = $widget;
  return $element;
}

/**
 * Implements hook_field_presave().
 */
function sugarcrm_contacts_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {
  // Flatten the array before saving.
  foreach ($items as &$item) {
    // Conditional fields might have already flattened the array.
    if (isset($item['contact'])) {
      $item = $item['contact'];
    }
  }
}

/**
 * Implements hook_field_widget_error().
 */
function sugarcrm_contacts_field_widget_error($element, $error, $form, &$form_state) {
  form_error($element, $error['message']);
}

/**
 * Returns the extra fields to display with the formatter.
 */
function sugarcrm_contacts_get_display_extra_fields($sugarid, $settings) {
  module_load_include('inc', 'sugarcrm_contacts', 'sugarcrm_contacts.sugar');
  $fields = _sugarcrm_contacts_get_fields($settings);
  $contact = sugarcrm_contacts_get_contact($sugarid, $fields);
  if (empty($contact)) {
    return array();
  }

  foreach ($fields as $f) {
    $field = $contact[$f];
    if (!empty($field['value'])) {
      $items[$f] = '<span class="sugarcrm-contacts-label">' . $field['label'] . ': </span>' .
        '<span class="sugarcrm-contacts-value">' . $field['value'] . '</span>';
    }
  }
  $element['extra_fields'] = array(
    '#theme' => 'item_list',
    '#type' => 'ul',
    '#attributes' => array('class' => 'sugarcrm-contacts-extra-fields'),
    '#items' => $items,
  );
  return $element;
}

/**
 * Returns the form elements to display the extra contact fields in the widget.
 */
function sugarcrm_contacts_get_widget_extra_fields($sugarid, $settings) {
  module_load_include('inc', 'sugarcrm_contacts', 'sugarcrm_contacts.sugar');
  $fields = _sugarcrm_contacts_get_fields($settings);
  $contact = sugarcrm_contacts_get_contact($sugarid, $fields);
  if (empty($contact)) {
    return array();
  }

  $form = array();
  foreach ($fields as $f) {
    $field = $contact[$f];
    if (!empty($field['value'])) {
      $form[$f] = array(
        '#type' => 'item',
        '#title' => $field['label'],
        '#markup' => '<span class="sugarcrm-contacts-value">' . $field['value'] . '</span>',
        '#prefix' => '<div class="sugarcrm-contacts-extra-field">',
        '#suffix' => '</div>',
      );
    }
  }
  return $form;
}

/**
 * Devuelve un array con los campos habilitados en las settings.
 */
function _sugarcrm_contacts_get_fields($settings) {
  $fields = array();
  foreach ($settings['contact_fields'] as $field) {
    if ($field) {
      $fields[] = $field;
    }
  }
  return $fields;
}
