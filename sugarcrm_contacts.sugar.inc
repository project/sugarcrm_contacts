<?php

/**
 * @file
 * Functions to interact with the SugarCRM instance.
 */

/**
 * Returns a SugarCRM wrapper already logged in, or FALSE in case of error.
 *
 * @param boolean $log_in
 *   if TRUE authenticate before returning the wrapper.
 */
function sugarcrm_contacts_get_wrapper($log_in = TRUE) {
  if (!($sugar = libraries_load('sugarcrm-wrapper')) || empty($sugar['installed'])) {
    watchdog('sugarcrm_contacts', 'SugarCRM Wrapper library not found.');
    return FALSE;
  }
  $url = variable_get('sugarcrm_url', FALSE);
  $user = variable_get('sugarcrm_user', FALSE);
  $pass = variable_get('sugarcrm_pass', FALSE);
  if (!$url || !$user || !$pass) {
    watchdog('sugarcrm_contacts', 'SugarCRM settings missing.');
    return FALSE;
  }

  $sugar = new \Asakusuma\SugarWrapper\Rest();
  $sugar->setUrl($url);
  $sugar->setUsername($user);
  $sugar->setPassword($pass);
  if ($log_in) {
    $sugar->connect();

    $error = $sugar->get_error();
    if ($error !== FALSE) {
      watchdog('sugarcrm_contacts', 'Error conecting to @url: @error.', array('@url' => $url, '@error' => $error['name']));
      return FALSE;
    }
  }

  return $sugar;
}

/**
 * Returns TRUE if $sugar_id is in the valid format for SugarCRM record id.
 *
 * Only for data sanitation, we don't check if such a record exists.
 */
function sugarcrm_contacts_is_valid_id($sugar_id) {
  $sugar = sugarcrm_contacts_get_wrapper(FALSE);
  return $sugar->is_valid_id($sugar_id);
}

/**
 * Search for contacts matching $search in SugarCRM.
 *
 * @returns array of contact names indexed by sugar id or FALSE en case of error
 */
function sugarcrm_contacts_search_contacts($search) {
  $sugar = sugarcrm_contacts_get_wrapper();
  if (!$sugar) {
    return;
  }
  $limit = intval(variable_get('sugarcrm_contacts_search_limit', 10));
  $search = $sugar->search_by_module($search, array('Contacts'), 0, $limit);
  return $search;
}

/**
 * Returns the sugar contact with id $sugarid.
 */
function sugarcrm_contacts_get_contact($sugarid, $fields) {
  $sugar = sugarcrm_contacts_get_wrapper();
  if (!$sugar) {
    return FALSE;
  }

  $options = array('where' => "contacts.id = '$sugarid'");
  $fields = _sugarcrm_contacts_prepare_fields($fields);
  $result = $sugar->get('Contacts', $fields, $options);
  if (empty($result)) {
    return FALSE;
  }
  $result = reset($result);
  $labels = _sugarcrm_contacts_sugar_fields();
  foreach ($result as $field => $value) {
    $contact[$field] = array(
      'label' => isset($labels[$field]) ? $labels[$field] : $field,
      'value' => $value,
    );
  }
  return $contact;
}

/**
 * Prepares the fields array for use in SugarWrapper::Rest::get.
 *
 * Adds default fields and expands address fields.
 */
function _sugarcrm_contacts_prepare_fields($fields) {
  if (!is_array($fields)) {
    $fields = array();
  }
  $select_fields = array('id');
  foreach ($fields as $field) {
    if ($field == 'primary_address' || $field == 'alt_address') {
      $select_fields[] = $field . '_street';
      $select_fields[] = $field . '_city';
      $select_fields[] = $field . '_state';
      $select_fields[] = $field . '_postalcode';
      $select_fields[] = $field . '_country';
    }
    else {
      $select_fields[] = $field;
    }
  }
  return $select_fields;
}

/**
 * Returns the available contact fields.
 */
function _sugarcrm_contacts_sugar_fields() {
  return array(
    'title' => t('Title'),
    'email1' => t('Primary Email'),
    'description' => t('Description'),
    'department' => t('Department'),
    'phone_home' => t('Home Phone'),
    'phone_mobile' => t('Mobile Phone'),
    'phone_work' => t('Work Phone'),
    'phone_other' => t('Other Phone'),
    'phone_fax' => t('Fax'),
    'do_not_call' => t('Do Not Call'),
    'primary_address' => t('Primary Address'),
    'other_address' => t('Other Address'),
    'assistant' => t('Assistant'),
    'assistant_phone' => t('Assistant Phone'),
    'birthdate' => t('Birthdate'),
  );
}
