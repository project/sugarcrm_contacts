SugarCRM Contacts
=================

SugarCRM Contacts as a drupal field (read only).

With this module you will be able to add a SugarCRM Contact Field to any content
type or entity.

Installation
------------

The usual stuff. Download the module, extract the file in your contrib modules
directory and enable it in admin/modules.

Or just

    $ drush en sugarcrm_contacts

You need to install https://github.com/asakusuma/SugarCRM-REST-API-Wrapper-Class
download and place it sites/all/libraries.

Configuration
-------------

To configure the SugarCRM connection go to Configuration > Webservices > SugarCRM.

You need to set the whole URL of the SugarCRM Webservices (not only the
the Sugar login url, i.e. http://my-sugarcrm.com/service/v2/rest.php), the username
and password.
You can also set the maximum number of results you want to get in the autocomplete.

Then add the field to your content type Or entity.

Use
---

To add a SugarCRM Contact in In the field the user have to write and click on search button to
connetc with SugarCRM. Then the user see the results and can click in
one of them.

Dependencies
------------

https://drupal.org/project/libraries
